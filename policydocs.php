<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Policy Documents</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php"><img src="image/logo2.png" alt="logo">Ethereal Beauty</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="signup.php">Sign Up</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header>
      
    </header>

    <!-- Page Content -->
    <div class="container">
    
          <h2>Privacy Policy</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
          <br>
          <hr>
          <br>
          <h4> <u> Information Collection and Use </u> </h4>
          <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quam magna, pharetra a tellus non, finibus dignissim purus. Aenean eu hendrerit nulla, eu pretium nisi. Morbi arcu ex, dictum vitae vehicula quis, fringilla quis quam. Sed posuere vel augue vel fermentum. Phasellus quis erat mi. Maecenas maximus elementum tellus vel malesuada. Praesent posuere, orci quis viverra pellentesque, est magna pretium lorem, dignissim ullamcorper nunc augue at elit. Pellentesque iaculis lacus non sapien rutrum interdum. Donec quam ipsum, tristique at tempus ultrices, finibus eget augue. Donec a velit id mi interdum lacinia in non diam. Nullam sit amet ipsum ac mauris tristique lobortis. Aenean eget quam congue nisi porta molestie. Pellentesque tincidunt, mi sed malesuada laoreet, nibh justo lacinia odio, ac luctus quam dolor nec turpis. Donec consectetur euismod rutrum. Donec sagittis laoreet ex at vehicula. </P>
          <br>
          <p>Pellentesque ac hendrerit justo, non fermentum eros. Mauris tempor gravida orci vel eleifend. Integer lacinia sagittis elit, vel lacinia orci hendrerit tincidunt. Etiam fermentum dignissim leo eu laoreet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed elementum vel ipsum eget tincidunt. Etiam quis eros neque. In hac habitasse platea dictumst. Praesent condimentum metus sed malesuada porttitor. Nullam molestie diam ut dui ultricies dapibus. </p>

          <h4><u>Log Data</u></h4>
          <p> Pellentesque ac hendrerit justo, non fermentum eros. Mauris tempor gravida orci vel eleifend. Integer lacinia sagittis elit, vel lacinia orci hendrerit tincidunt. Etiam fermentum dignissim leo eu laoreet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed elementum vel ipsum eget tincidunt. Etiam quis eros neque. In hac habitasse platea dictumst. Praesent condimentum metus sed malesuada porttitor. Nullam molestie diam ut dui ultricies dapibus.  </p>
        
          <h4><u>Communication</u></h4>
          <p>Pellentesque ac hendrerit justo, non fermentum eros. Mauris tempor gravida orci vel eleifend. Integer lacinia sagittis elit, vel lacinia orci hendrerit tincidunt. Etiam fermentum dignissim leo eu laoreet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed elementum vel ipsum eget tincidunt. Etiam quis eros neque. In hac habitasse platea dictumst. Praesent condimentum metus sed malesuada porttitor. Nullam molestie diam ut dui ultricies dapibus. </p>
           
          <h4><u>Cookies</u></h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quam magna, pharetra a tellus non, finibus dignissim purus. Aenean eu hendrerit nulla, eu pretium nisi. Morbi arcu ex, dictum vitae vehicula quis, fringilla quis quam. Sed posuere vel augue vel fermentum. Phasellus quis erat mi. Maecenas maximus elementum tellus vel malesuada. Praesent posuere, orci quis viverra pellentesque, est magna pretium lorem, dignissim ullamcorper nunc augue at elit. Pellentesque iaculis lacus non sapien rutrum interdum. Donec quam ipsum, tristique at tempus ultrices, finibus eget augue. Donec a velit id mi interdum lacinia in non diam. Nullam sit amet ipsum ac mauris tristique lobortis. Aenean eget quam congue nisi porta molestie. Pellentesque tincidunt, mi sed malesuada laoreet, nibh justo lacinia odio, ac luctus quam dolor nec turpis. Donec consectetur euismod rutrum. Donec sagittis laoreet ex at vehicula. </p>
          
          <h4><u>Security</u></h4>
          <p>Pellentesque ac hendrerit justo, non fermentum eros. Mauris tempor gravida orci vel eleifend. Integer lacinia sagittis elit, vel lacinia orci hendrerit tincidunt. Etiam fermentum dignissim leo eu laoreet. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed elementum vel ipsum eget tincidunt. Etiam quis eros neque. In hac habitasse platea dictumst. Praesent condimentum metus sed malesuada porttitor. Nullam molestie diam ut dui ultricies dapibus. Suspendisse commodo ipsum sed est rhoncus, at aliquet nulla aliquam. Donec at laoreet neque, et convallis massa. Maecenas suscipit turpis tellus, vitae efficitur elit sodales et. Sed id libero vel felis mattis dignissim. Nam sodales, lacus congue tincidunt pharetra, leo quam placerat velit, eget luctus leo felis in ante. </p>

          <h4><u>Contact Us</u></h4>
          <p> If you have any questions about this privacy policy, please contact us</p>
          <a href="contact.php">Contact Us</a>
          <<a href="pdf/policydoc.pdf">Download Policy Document</a>
          <br>
          <br>
    </div>
    <hr>
   
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Ethereal Beauty 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
